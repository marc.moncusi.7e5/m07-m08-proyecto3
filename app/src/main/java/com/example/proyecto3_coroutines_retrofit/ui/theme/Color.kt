package com.example.proyecto3_coroutines_retrofit.ui.theme

import androidx.compose.ui.graphics.Color

val Dark = Color(0xFF2D2424)
val Brown = Color(0xFF5C3D2E)
val Orange = Color(0xFFB85C38)
val Beige = Color(0xFFE0C097)
val Sage = Color(0xFFABC4AA)