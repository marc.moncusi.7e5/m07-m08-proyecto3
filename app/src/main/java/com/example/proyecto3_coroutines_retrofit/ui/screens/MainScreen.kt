package com.example.proyecto3_coroutines_retrofit.ui.screens

import android.content.Intent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.example.proyecto3_coroutines_retrofit.DetailActivity
import com.example.proyecto3_coroutines_retrofit.R
import com.example.proyecto3_coroutines_retrofit.model.CatDetails
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

@Composable
fun MainScreen(
  catsUIState: CatsUIState,
  SetSelectedCountry: (String) -> Unit
){
  when(catsUIState){
    is CatsUIState.Loading -> LoadingScreen()
    is CatsUIState.Success -> ResultScreen(
      catsUIState.photos,
      catsUIState.SelectCountry,
      SetSelectedCountry
    )
    is CatsUIState.Error -> ErrorScreen()
  }
}
@Composable
fun LoadingScreen() {
  val boxModifier = Modifier
    .fillMaxSize()
    .background(color = MaterialTheme.colors.background)
  val boxImageModifier = Modifier
    .fillMaxWidth()
  Box(modifier = boxModifier){
    Image(
      modifier = boxImageModifier,
      painter = painterResource(R.drawable.processing),
      contentDescription = stringResource(id = R.string.loading)
    )
  }
}

@Composable
fun ResultScreen(
  catDetailsList: List<CatDetails>,
  country : String,
  setSelectedCountry: (String) -> Unit
){
  val spacerModifier = Modifier.
    height(3.dp)
  Column {
    var catListFiltered = catDetailsList
    CountrySelector(country, setSelectedCountry)
    if(country != "Select Country"){
      catListFiltered = catDetailsList.filter { it.countryCode == country }
    }
    Spacer(modifier = spacerModifier)
    CatList(catListFiltered)
  }
}

@Composable
fun ErrorScreen() {
  val boxModifier = Modifier
    .fillMaxSize()
    .background(color = MaterialTheme.colors.background)
  val boxImageModifier = Modifier
    .fillMaxWidth()
  Box(modifier = boxModifier){
    Image(
      modifier = boxImageModifier,
      painter = painterResource(R.drawable.error),
      contentDescription = stringResource(id = R.string.loading)
    )
  }
}

@Composable
fun CountrySelector(Country: String, setSelectedCountry: (String) -> Unit){
  val listOfCountries = arrayOf(
    "Select Country",
    "AE",
    "AU",
    "BR",
    "CA",
    "CN",
    "CY",
    "EG",
    "ES",
    "FR",
    "GB",
    "GR",
    "IR",
    "JP",
    "MM",
    "RU",
    "SO",
    "SP",
    "TH",
    "TR",
    "US",
  )
  val rowModifier = Modifier
    .fillMaxWidth()
    .padding(5.dp)

  val dropdownModifier = Modifier
    .height(300.dp)

  var expanded by remember {
    mutableStateOf(false)
  }
  Row(
    modifier = rowModifier,
    verticalAlignment = Alignment.Top,
    horizontalArrangement = Arrangement.Start
  ){
    Button(
      onClick = { expanded = true },
      shape = MaterialTheme.shapes.large){
      Text(Country)
      Icon(
        imageVector = Icons.Default.ArrowDropDown,
        contentDescription = stringResource(id = R.string.expand_options),
      )
    }
    DropdownMenu(
      expanded = expanded,
      onDismissRequest = { expanded = false },
      modifier = dropdownModifier
    ) {
      listOfCountries.forEach { countryCodeElement ->
        DropdownMenuItem(
          onClick = {
            setSelectedCountry(countryCodeElement)
            expanded = false
                    },
          enabled = (countryCodeElement != Country)
        ) {
          Text(countryCodeElement)
        }
      }
    }
  }
}

@Composable
fun CatList(catList: List<CatDetails>,modifier: Modifier = Modifier){
  val mContext = LocalContext.current
  LazyColumn(modifier = modifier) {
    items(items = catList){
      catDetails ->
        Button(
          onClick = {
            val intent = Intent(mContext, DetailActivity::class.java)
            val detailsStr = Json.encodeToString(catDetails)
            intent.putExtra("CatDetails", detailsStr)
            mContext.startActivity(intent)
          }
        ){
          CatCard(
            catDetails
          )
        }
      Spacer(Modifier.height(2.dp))
    }
  }
}

@Composable
fun CatCard(catPhoto: CatDetails){
  val imageModifier = Modifier
    .fillMaxWidth()
    .height(250.dp)
    .border(BorderStroke(1.dp, MaterialTheme.colors.secondary))
  Card(modifier = Modifier, elevation = 1.dp){
    Column{
      Box(modifier = Modifier.fillMaxWidth(),
      contentAlignment = Alignment.BottomStart
      ){
        GlideImage(
          imageModel = { catPhoto.image!!.url },
          modifier = imageModifier,
          imageOptions = ImageOptions(contentScale = ContentScale.Crop)
        )
        Row(
          Modifier.fillMaxWidth()
        ){
          Text(
            text = "Breed:",
            style = MaterialTheme.typography.h6
          )
          Spacer(Modifier.weight(1f))
          catPhoto.name?.let { Text(text = it, style = MaterialTheme.typography.h6) }
        }
      }
    }
  }
}
