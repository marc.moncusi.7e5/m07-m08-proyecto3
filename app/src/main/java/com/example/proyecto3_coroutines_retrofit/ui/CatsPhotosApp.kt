package com.example.proyecto3_coroutines_retrofit.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.proyecto3_coroutines_retrofit.R
import com.example.proyecto3_coroutines_retrofit.ui.screens.CatViewModel
import com.example.proyecto3_coroutines_retrofit.ui.screens.MainScreen

@Composable
fun CatsPhotosApp(modifier: Modifier = Modifier) {
  Scaffold (
    modifier = modifier.fillMaxSize(),
    topBar = {
      TopAppBar(title = { Text(stringResource(id = R.string.app_name)) })
    }
  ) {
    Surface(
      modifier = Modifier
        .fillMaxSize()
        .padding(it),
    ){
      val catsViewModel: CatViewModel = viewModel()
      MainScreen(
        catsUIState = catsViewModel.catsUiState
      ) {
        catsViewModel.SetSelectedCountry(it)
      }
    }
  }
}