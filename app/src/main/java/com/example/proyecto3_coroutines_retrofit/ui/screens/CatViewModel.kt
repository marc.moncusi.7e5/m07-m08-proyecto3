package com.example.proyecto3_coroutines_retrofit.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.proyecto3_coroutines_retrofit.model.CatDetails
import com.example.proyecto3_coroutines_retrofit.network.Api
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

/**
 * UI state for the Home Screen
 */
sealed interface CatsUIState {
  data class Success(val photos: List<CatDetails>, val SelectCountry: String) : CatsUIState
  object Error : CatsUIState
  object Loading : CatsUIState
}

class CatViewModel : ViewModel() {
  /** The mutable State that stores the status of the most recent request*/
  var catsUiState: CatsUIState by mutableStateOf(CatsUIState.Loading)
    private set

  init {
    getCatsPhotos()
  }

  fun getCatsPhotos() {
    viewModelScope.launch {
      catsUiState = try {
        val listResult = Api.retrofitService.getCatDetails()
        CatsUIState.Success(listResult, "Select Country")
      } catch (e: IOException){
        CatsUIState.Error
      } catch (e: HttpException) {
        CatsUIState.Error
      }
    }
  }
  fun SetSelectedCountry(newSelectedCountry: String){
    if(catsUiState is CatsUIState.Success){
      catsUiState = (catsUiState as CatsUIState.Success).copy(SelectCountry = newSelectedCountry)
    }
  }
}