package com.example.proyecto3_coroutines_retrofit

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import com.example.proyecto3_coroutines_retrofit.model.CatDetails
import com.example.proyecto3_coroutines_retrofit.ui.theme.Proyecto3_Coroutines_RetrofitTheme
import com.skydoves.landscapist.ImageOptions
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

class DetailActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    val intent = intent
    val detailsStr = intent.getStringExtra("CatDetails")
    val details = detailsStr?.let { Json.decodeFromString<CatDetails>(it) }

    val ColumnModifier = Modifier
      .fillMaxSize()
    setContent {
      Proyecto3_Coroutines_RetrofitTheme {
        Surface(
          modifier = Modifier.fillMaxSize()
        ){
          Column(
            modifier = ColumnModifier,
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally,
          ){
            if(details != null){
              DetailedCatCard(details)
            }
          }
        }
      }
    }
  }
}

@Composable
fun DetailedCatCard(details: CatDetails){
  val imageModifier = Modifier
    .height(250.dp)
    .padding(3.dp)
    .fillMaxWidth()
  val subtitleModifier = Modifier
    .fillMaxWidth()
    .padding(0.dp, 5.dp)
  Column() {
    Box(modifier = Modifier.fillMaxWidth(),
      contentAlignment = Alignment.BottomStart
    ) {
      GlideImage(
        imageModel = { details.image!!.url },
        modifier = imageModifier,
        imageOptions = ImageOptions(contentScale = ContentScale.Crop)
      )
      Row(
        Modifier.fillMaxWidth()
      ) {
        details.name?.let { Text(
          text = it,
          style = MaterialTheme.typography.h5
        ) }
      }
    }
    Text(
      text = "Description",
      fontWeight = FontWeight.Bold,
      modifier = subtitleModifier
    )
    details.description?.let { Text(text = it) }
    Text(
      text = "Country Code",
      fontWeight = FontWeight.Bold,
      modifier = subtitleModifier
    )
    details.countryCode?.let { Text(text = it) }
    Text(
      text = "Temperament",
      fontWeight = FontWeight.Bold,
      modifier = subtitleModifier
    )
    details.temperament?.let { Text(text = it) }
    Text(
      text = "Wikipedia Url",
      fontWeight = FontWeight.Bold,
      modifier = subtitleModifier
    )
    details.wikipediaUrl?.let { MyButton(Url = it) }
  }
}

@Composable
fun MyButton(Url: String) {
  val context = LocalContext.current
  val intent = remember {
    Intent(Intent.ACTION_VIEW, Uri.parse(Url))
  }
  Box(
    modifier = Modifier.clickable { context.startActivity(intent) },
  ) {
    Text(
      text = Url
    )
  }
}
