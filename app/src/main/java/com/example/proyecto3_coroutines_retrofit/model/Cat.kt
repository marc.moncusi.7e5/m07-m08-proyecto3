package com.example.proyecto3_coroutines_retrofit.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
/**
 * [CatDetails] is the data class to represent the id Affirmation text and imageResourceId
 */

@Serializable
data class CatDetails(
  @SerialName("id") val id: String? = null,
  @SerialName("name") val name: String? = null,
  @SerialName("description") val description: String? = null,
  @SerialName("country_code") val countryCode: String? = null,
  @SerialName("temperament") val temperament: String? = null,
  @SerialName("wikipedia_url") val wikipediaUrl: String? = null,
  @SerialName("image") val image: CatImg? = CatImg(),
)
@Serializable
data class CatImg(
  @SerialName("id") val id: String? = null,
  @SerialName("url") val url: String? = null,
  @SerialName("width") val width: Int? = null,
  @SerialName("height") val height: Int? = null
)
