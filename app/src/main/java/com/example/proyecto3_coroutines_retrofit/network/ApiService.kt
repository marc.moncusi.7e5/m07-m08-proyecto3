package com.example.proyecto3_coroutines_retrofit.network

import com.example.proyecto3_coroutines_retrofit.model.CatDetails
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json

import okhttp3.MediaType.Companion.toMediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Headers

private const val BASE_URL = "https://api.thecatapi.com/v1/"

private const val token = "live_mMSCCx6D0BSilvhlX8AgWfJnZuQueCzB9ssXF1Xi0sqULjdx40iskZ8UJe5XKH48"

private val jsonIgnored = Json {ignoreUnknownKeys = true}

private val retrofit = Retrofit.Builder()
  .addConverterFactory(jsonIgnored.asConverterFactory("application/json".toMediaType()))
  .baseUrl(BASE_URL)
  .build()

/**
 * Use the Retrofit builder to build a retrofit object using a kotlinx.serialization converter
 */
interface ApiService {
  @Headers("x-api-key: $token")
  @GET("breeds")
  suspend fun getCatDetails(): List<CatDetails>
}

/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object Api {
  val retrofitService: ApiService by lazy {
    retrofit.create(ApiService::class.java)
  }
}