package com.example.proyecto3_coroutines_retrofit

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Surface
import androidx.compose.ui.Modifier
import com.example.proyecto3_coroutines_retrofit.ui.CatsPhotosApp
import com.example.proyecto3_coroutines_retrofit.ui.theme.Proyecto3_Coroutines_RetrofitTheme

class CatsPhotosActivity: ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      Proyecto3_Coroutines_RetrofitTheme {
        Surface(
          modifier = Modifier.fillMaxSize()
        ){
          CatsPhotosApp()
        }
      }
    }
  }
}