package com.example.proyecto3_coroutines_retrofit

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.text.input.*
import androidx.compose.ui.unit.dp
import com.example.proyecto3_coroutines_retrofit.ui.theme.Proyecto3_Coroutines_RetrofitTheme


class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      Proyecto3_Coroutines_RetrofitTheme {
        Surface(
          modifier = Modifier.fillMaxSize()
        ){
          ShowLogIn()
        }
      }
    }
  }
}
@Composable
fun ShowLogIn() {

  Column(
    modifier = Modifier
      .fillMaxWidth()
      .background(color = colors.primary),
    verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally,

    ) {
    Text(
      modifier = Modifier.padding(top = 10.dp),
      text = "User",
    )

    val focusManager = LocalFocusManager.current

    EmailInput(focusManager)
    Text(text = "Password")
    PasswordInput(focusManager)
    LogInButton()


  }

}

@Composable
fun EmailInput(focusManager: FocusManager) {

  var emailText by remember { mutableStateOf(TextFieldValue("")) }
  OutlinedTextField(
    value = emailText,
    onValueChange = { emailText = it },
    colors = TextFieldDefaults.outlinedTextFieldColors(
      focusedBorderColor = colors.primaryVariant,
      textColor = Black
    ),
    label = { Text("Email", color = colors.secondary) },
    singleLine = true,
    modifier = Modifier.fillMaxWidth(),
    keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
    keyboardOptions = KeyboardOptions.Default.copy(
      imeAction = ImeAction.Next,
      keyboardType = KeyboardType.Email
    ), isError= android.util.Patterns.EMAIL_ADDRESS.matcher(emailText.text).matches()

  )
}

@Composable
fun PasswordInput(focusManager: FocusManager) {
  var passwordText by remember { mutableStateOf(TextFieldValue("")) }
  var passwordVisible by remember { mutableStateOf(false) }

  OutlinedTextField(

    value = passwordText,
    onValueChange = { passwordText = it },
    label = { Text("Password",color = colors.secondary) },
    colors = TextFieldDefaults.outlinedTextFieldColors(
      focusedBorderColor = colors.primaryVariant,
      textColor = Black
    ),
    singleLine = true,
    modifier = Modifier.fillMaxWidth(),
    keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
    keyboardOptions = KeyboardOptions.Default.copy(
      imeAction = ImeAction.Done,
      keyboardType = KeyboardType.Password
    ),
    visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
    trailingIcon = {
      val image =
        if (passwordVisible) Icons.Filled.Visibility else Icons.Filled.VisibilityOff
      val description = if (passwordVisible) "Hide password" else "Show Password"
      IconButton(onClick = { passwordVisible = !passwordVisible }) {
        Icon(imageVector = image, description)
      }
    }
  )
}

@Composable
fun LogInButton() {
  val context = LocalContext.current

  Button(
    colors = ButtonDefaults.buttonColors(
      backgroundColor = colors.secondary,
      contentColor = colors.background
    ),
    onClick = {
      val intent = Intent(context, CatsPhotosActivity::class.java)
      context.startActivity(intent)
    }, modifier = Modifier.padding(top = 30.dp)
  ) {
    Text(text = "Log in ")
  }
}
